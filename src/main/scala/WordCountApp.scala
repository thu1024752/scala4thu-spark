import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac047 on 2017/5/8.
  */
object WordCountApp extends App{

  val conf = new SparkConf().setAppName("WordCount")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)
  val lines=sc.textFile("/Users/mac047/Downloads/spark-doc.txt")
  lines.take(10).foreach(println)
  lines.flatMap(str=>str.split(" ").toList).take(10).foreach(println)
  val words=lines.flatMap(str=>str.split(" "))

  words.map(word=>word->1).reduceByKey((acc,curr)=>acc+curr)
  words.groupBy(str=>str).mapValues(_.size).take(10).foreach(println)
  List("Mark","Spark","RDD","RDD").groupBy(str=>str).mapValues(_.size)
readLine()

}
